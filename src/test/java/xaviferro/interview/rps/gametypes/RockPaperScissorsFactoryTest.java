package xaviferro.interview.rps.gametypes;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import xaviferro.interview.rps.GameResultType;
import xaviferro.interview.rps.Gesture;
import xaviferro.interview.rps.GestureGame;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class RockPaperScissorsFactoryTest {
    private GestureGame rockPaperScissors;
    private RockPaperScissorsFactory factory;

    @BeforeTest
    public void before() {
        factory = new RockPaperScissorsFactory();
        rockPaperScissors = factory.newGame();
    }

    @Test
    public void rockIsDefined() {
        rockPaperScissors.getGesture("rock").get();
    }

    @Test
    public void paperIsDefined() {
        rockPaperScissors.getGesture("paper").get();
    }

    @Test
    public void scissorsIsDefined() {
        rockPaperScissors.getGesture("scissors").get();
    }

    @Test
    public void rockDrawsRock() {
        Gesture rock = rockPaperScissors.getGesture("rock").get();
        Assert.assertEquals(rock.versus(rock), GameResultType.DRAW);
        assertEquals(rock.beatAction(rock), Optional.empty());
    }

    @Test
    public void rockWinsScissors() {
        Gesture rock = rockPaperScissors.getGesture("rock").get();
        Gesture scissors = rockPaperScissors.getGesture("scissors").get();

        assertEquals(rock.versus(scissors), GameResultType.WIN);
        assertEquals(rock.beatAction(scissors), Optional.of("crushes"));
    }

    @Test
    public void rockLossesVersusPaper() {
        Gesture rock = rockPaperScissors.getGesture("rock").get();
        Gesture paper = rockPaperScissors.getGesture("paper").get();

        assertEquals(rock.versus(paper), GameResultType.LOSE);
    }

    @Test
    public void scissorsDrawsScissors() {
        Gesture scissors = rockPaperScissors.getGesture("scissors").get();

        assertEquals(scissors.versus(scissors), GameResultType.DRAW);
    }

    @Test
    public void scissorsWinsPaper() {
        Gesture scissors = rockPaperScissors.getGesture("scissors").get();
        Gesture paper = rockPaperScissors.getGesture("paper").get();

        assertEquals(scissors.versus(paper), GameResultType.WIN);
        assertEquals(scissors.beatAction(paper), Optional.of("cuts"));
    }

    @Test
    public void scissorsLosesVsRock() {
        Gesture rock = rockPaperScissors.getGesture("rock").get();
        Gesture scissors = rockPaperScissors.getGesture("scissors").get();

        assertEquals(scissors.versus(rock), GameResultType.LOSE);
    }

    @Test
    public void paperDrawsPaper() {
        Gesture paper = rockPaperScissors.getGesture("paper").get();

        assertEquals(paper.versus(paper), GameResultType.DRAW);
    }

    @Test
    public void paperWinsRock() {
        Gesture rock = rockPaperScissors.getGesture("rock").get();
        Gesture paper = rockPaperScissors.getGesture("paper").get();

        assertEquals(paper.versus(rock), GameResultType.WIN);
        assertEquals(paper.beatAction(rock), Optional.of("covers"));
    }

    @Test
    public void paperLosesVsScissors() {
        Gesture scissors = rockPaperScissors.getGesture("scissors").get();
        Gesture paper = rockPaperScissors.getGesture("paper").get();

        assertEquals(paper.versus(scissors), GameResultType.LOSE);
    }
}
