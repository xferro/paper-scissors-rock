package xaviferro.interview.rps;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

public class GestureGameBuilderTest {
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void tryingToAssignToNonDefinedGestureThrowsAnException() {
        GestureGameBuilder builder = new GestureGameBuilder("test");
        builder.addGesture("source", new GestureAction("fails", "miserably"));
        builder.build();
    }

    @Test
    public void nameIsDefined() {
        GestureGame game = new GestureGameBuilder("test").build();
        Assert.assertEquals(game.getName(), "test");
    }

    @Test
    public void getGestureReturnsEmptyIfNotDefined() {
        GestureGame game = new GestureGameBuilder("test").build();
        Assert.assertEquals(game.getGesture("fail"), Optional.empty());
    }

    @Test
    public void actionsAreDefined() {
        GestureGameBuilder builder = new GestureGameBuilder("test");
        GestureGame game = builder
                .addGesture("source", new GestureAction("goes", "target"))
                .addGesture("target", new GestureAction("back", "source"))
                .build();

        Gesture source = game.getGesture("source").get();
        Gesture target = game.getGesture("target").get();

        Assert.assertEquals(game.getGestures().size(), 2);
        Assert.assertEquals(source.beatAction(target), Optional.of("goes"));
        Assert.assertEquals(target.beatAction(source), Optional.of("back"));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void nameCannotBeNull() {
        new GestureGameBuilder(null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void gestureNameCannotBeNull() {
        GestureGameBuilder builder = new GestureGameBuilder("test");
        builder.addGesture(null, new GestureAction("goes", "target"));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void gestureActionsCannotBeNull() {
        GestureGameBuilder builder = new GestureGameBuilder("test");
        builder.addGesture("hello", null);
    }
}
