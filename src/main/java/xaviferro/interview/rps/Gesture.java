package xaviferro.interview.rps;

import java.util.Optional;

/**
 * Represents a hand.
 */
public interface Gesture {
    /** Some fancy name */
    String getName();

    /** What is the result of competing */
    GameResultType versus(Gesture gesture);

    /**
     * Returns how we beat gesture.
     *
     * @return the action name if we can bet gesture. Otherwise, an empty optional.
     */
    Optional<String> beatAction(Gesture gesture);
}
