/**
 * Basic implementation for Rock/Paper/Scissors game.
 *
 * <p>
 * I have implemented some extensibility to the GestureGame and few helper classes for
 * building any future gesture games (programmatically or we could be processing external configuration
 * files) like {@link xaviferro.interview.rps.GestureGameBuilder}
 * and {@link xaviferro.interview.rps.GestureGameFactory}.
 * </p>
 *
 * <p>
 * Some hierarchies could have been more developed (like Player or defining an Engine), but
 * for the purpose of the exercise it seemed enough.
 * </p>
 */
package xaviferro.interview.rps;