package xaviferro.interview.rps;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

final class GestureGameImpl implements GestureGame {
    private final String name;
    private final String description;
    private final Map<String, Gesture> gestures;
    private final List<Gesture> list;

    GestureGameImpl(String name, String description, Set<Gesture> gestures) {
        this.name = name;
        this.description = description;
        this.gestures = gestures.stream().collect(Collectors.toMap(Gesture::getName, Function.identity()));
        this.list = Collections.unmodifiableList(new ArrayList<>(this.gestures.values()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Gesture> getGestures() {
        return list;
    }

    @Override
    public Optional<Gesture> getGesture(String name) {
        return Optional.ofNullable(gestures.get(name));
    }
}
