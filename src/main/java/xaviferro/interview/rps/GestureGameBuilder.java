package xaviferro.interview.rps;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * We build {@link GestureGame}s to support as there maybe multiple gesture games availables.
 *
 * <p>
 * This class allows building gesture games. Anyone could create a game and configure the names/rules
 * programmatically and/or from an external file if desired.
 * </p>
 */
public class GestureGameBuilder {
    private String name;
    private String description;
    private Map<String, Set<GestureAction>> rules;

    public GestureGameBuilder(String name) {
        requireNonNull(name, "Name cannot be null");

        this.name = name;
        this.rules = Maps.newHashMap();
    }

    public GestureGameBuilder description(String description) {
        this.description = description;
        return this;
    }

    public GestureGameBuilder addGesture(String name, GestureAction ... actions) {
        requireNonNull(name, "Name cannot be null");
        requireNonNull(actions, "Actions cannot be null");

        rules.put(name, Sets.newHashSet(actions));
        return this;
    }

    public GestureGame build() {
        // First we build the gestures
        Map<String, GestureImpl> gestures = rules.keySet().stream()
                .collect(Collectors.toMap(name -> name, name -> new GestureImpl(name)));

        // Later, we assign actions to gestures assuring gesture within actions exist
        for (Map.Entry<String, GestureImpl> entry : gestures.entrySet()) {
            Set<GestureAction> gestureActions = rules.get(entry.getKey());
            for (GestureAction gestureAction: gestureActions) {
                Optional<GestureImpl> targetGesture = Optional.ofNullable(gestures.get(gestureAction.getGesture()));
                entry.getValue().beats(gestureAction.getAction(), targetGesture.orElseThrow(() -> new IllegalArgumentException("Gesture " + gestureAction.getGesture() + " defined in action does not exist")));
            }
        }

        return new GestureGameImpl(name, description, ImmutableSet.copyOf(gestures.values()));
    }
}
