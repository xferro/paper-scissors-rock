package xaviferro.interview.rps;

public enum GameResultType {
    WIN,
    LOSE,
    DRAW,
}
