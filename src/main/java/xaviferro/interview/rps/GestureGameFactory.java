package xaviferro.interview.rps;

/**
 * We might have multiple games that we'd like to play.
 */
public interface GestureGameFactory {
    GestureGame newGame();
}
