package xaviferro.interview.rps;

import java.util.Objects;

/**
 * Defines a winner action over another gesture.
 */
public class GestureAction {
    private final String action;
    private final String gesture;

    public GestureAction(String action, String gesture) {
        this.action = action;
        this.gesture = gesture;
    }

    public String getGesture() {
        return gesture;
    }

    public String getAction() {
        return action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GestureAction that = (GestureAction) o;
        return Objects.equals(action, that.action) &&
                Objects.equals(gesture, that.gesture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action, gesture);
    }
}
