package xaviferro.interview.rps;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Optional;

public class GestureImpl implements Gesture {
    private final String name;
    private Map<Gesture, String> beats;

    public GestureImpl(String name) {
        this.name = name;
        this.beats = Maps.newHashMap();
    }

    public GestureImpl beats(String action, Gesture gesture) {
        beats.put(gesture, action);
        return this;
    }

    @Override
    public Optional<String> beatAction(Gesture gesture) {
        return Optional.ofNullable(beats.get(gesture));
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public GameResultType versus(Gesture gesture) {
        if (gesture.equals(this))
            return GameResultType.DRAW;

        if (beats.containsKey(gesture))
            return GameResultType.WIN;

        return GameResultType.LOSE;
    }
}
