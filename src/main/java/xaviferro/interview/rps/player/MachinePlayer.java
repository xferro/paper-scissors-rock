package xaviferro.interview.rps.player;

import xaviferro.interview.rps.Gesture;
import xaviferro.interview.rps.GestureGame;

import java.util.Random;

public class MachinePlayer implements Player {
    private final Random rand;
    private final String name;

    public MachinePlayer(String name) {
        this.name = name;
        this.rand = new Random();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Gesture play(GestureGame game) {
        int next = rand.nextInt(game.getGestures().size());
        return game.getGestures().get(next);
    }
}
