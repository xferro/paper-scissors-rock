package xaviferro.interview.rps.player;

import xaviferro.interview.rps.Gesture;
import xaviferro.interview.rps.GestureGame;

/**
 * Represents a player in the game.
 */
public interface Player {
    String getName();
    Gesture play(GestureGame game);
}
