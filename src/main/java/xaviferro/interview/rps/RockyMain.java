package xaviferro.interview.rps;

import xaviferro.interview.rps.gametypes.RockPaperScissorsFactory;
import xaviferro.interview.rps.gametypes.RockPaperScissorsLizardSpockFactory;
import xaviferro.interview.rps.player.MachinePlayer;
import xaviferro.interview.rps.player.Player;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Very basic console management for running the games. Devs shouldn't code UIs.
 */
public class RockyMain {
    private GestureGame rockPaperScissors;
    private GestureGame rockPaperScissorsLizardSpock;
    private Player machinePlayer;

    private Scanner scanner = new Scanner(System.in);

    public RockyMain() {
        rockPaperScissors = new RockPaperScissorsFactory().newGame();
        rockPaperScissorsLizardSpock = new RockPaperScissorsLizardSpockFactory().newGame();
        machinePlayer = new MachinePlayer("R2D2");

        inputMenu();
    }

    void inputMenu() {
        boolean keepPlaying;
        do {
            GestureGame game = chooseGame();
            Gesture gesture = chooseGesture(game);
            Gesture opponent = machinePlayer.play(game);
            keepPlaying = showResult(gesture, opponent);
        } while (keepPlaying);
    }

    public boolean showResult(Gesture player, Gesture opponent) {
        GameResultType result = player.versus(opponent);
        StringBuffer message = new StringBuffer("You picked " + player.getName() + " and opponent picked " + opponent.getName() + " so you " + result.name());
        if (result == GameResultType.LOSE) {
            message.append(" because " + opponent.getName() + " " + opponent.beatAction(player).get() + " " + player.getName());
        } else if (result == GameResultType.WIN) {
            message.append(" because " + player.getName() + " " + player.beatAction(opponent).get() + " " + opponent.getName());
        }
        message.append("\nDo you want to keep playing?");
        message.append("\n0. Exit please");
        message.append("\n1. Continue");
        System.out.println(message);
        int input = scanner.nextInt();
        return input != 0;
    }

    public GestureGame chooseGame() {
        boolean exit = false;
        GestureGame game = null;

        do {
            StringBuffer message = new StringBuffer("Please choose games");
            message.append("\n1. " + rockPaperScissors.getName());
            message.append("\n2. " + rockPaperScissorsLizardSpock.getName());

            System.out.println(message);

            int input = scanner.nextInt();
            switch (input) {
                case 1: game = rockPaperScissors;
                        exit = true;
                        break;
                case 2: game = rockPaperScissorsLizardSpock;
                        exit = true;
                        break;
            }
        } while (!exit);

        return game;
    }

    public Gesture chooseGesture(GestureGame game){
        boolean exit = false;
        Gesture gesture = null;
        do {
            StringBuffer message = new StringBuffer("Please choose your hand");
            IntStream.range(0, game.getGestures().size())
                    .forEach(idx -> {
                        message.append("\n" + idx + " " + game.getGestures().get(idx).getName());
                    });
            System.out.println(message);

            int input = scanner.nextInt();
            if (input < game.getGestures().size() && input >= 0) {
                gesture = game.getGestures().get(input);
                exit = true;
            }
        } while (!exit);

        return gesture;
    }

    public static void main(String[] args) {
        new RockyMain();
    }
}
