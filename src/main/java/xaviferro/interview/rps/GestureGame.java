package xaviferro.interview.rps;

import java.util.List;
import java.util.Optional;

/**
 * Contains the definition of a gesture game which is the set of gestures plus some meta information.
 */
public interface GestureGame {
    /** Name of the game */
    String getName();

    /** Fancy description of the game */
    String getDescription();

    /** List of gestures that a player could pick */
    List<Gesture> getGestures();

    /** Returns the gesture by name of Optional.empty it was not defined */
    Optional<Gesture> getGesture(String name);
}
