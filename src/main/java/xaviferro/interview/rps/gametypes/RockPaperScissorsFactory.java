package xaviferro.interview.rps.gametypes;

import xaviferro.interview.rps.GestureAction;
import xaviferro.interview.rps.GestureGame;
import xaviferro.interview.rps.GestureGameBuilder;
import xaviferro.interview.rps.GestureGameFactory;

public class RockPaperScissorsFactory implements GestureGameFactory {
    @Override
    public GestureGame newGame() {
        return new GestureGameBuilder("Rock Paper Scissors")
                .description("Traditional rock paper scissors game")
                .addGesture("rock", new GestureAction("crushes", "scissors"))
                .addGesture("scissors", new GestureAction("cuts", "paper"))
                .addGesture("paper", new GestureAction("covers","rock"))
                .build();
    }
}
