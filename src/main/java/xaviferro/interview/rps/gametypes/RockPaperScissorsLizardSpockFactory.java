package xaviferro.interview.rps.gametypes;

import xaviferro.interview.rps.GestureAction;
import xaviferro.interview.rps.GestureGame;
import xaviferro.interview.rps.GestureGameBuilder;
import xaviferro.interview.rps.GestureGameFactory;

/**
 * As Sheldon explains, "Scissors cuts paper, paper covers rock, rock crushes lizard, lizard poisons Spock,
 * Spock smashes scissors, scissors decapitates lizard, lizard eats paper, paper disproves Spock,
 * Spock vaporizes rock, and as it always has, rock crushes scissors."
 */
public class RockPaperScissorsLizardSpockFactory implements GestureGameFactory {
    @Override
    public GestureGame newGame() {
        return new GestureGameBuilder("Rock Paper Scissors Spock Lizard")
                .description("This is a weird game")
                .addGesture("rock",
                        new GestureAction("crushes", "scissors"),
                        new GestureAction("crushes", "lizard"))
                .addGesture("scissors",
                        new GestureAction("cuts", "paper"),
                        new GestureAction("decapitates","lizard"))
                .addGesture("paper",
                        new GestureAction("covers","rock"),
                        new GestureAction("disproves","spock"))
                .addGesture("lizard",
                        new GestureAction("poisons","spock"),
                        new GestureAction("eats","paper"))
                .addGesture("spock",
                        new GestureAction("smashes","scissors"),
                        new GestureAction("vaporizes","rock"))
                .build();
    }
}
